/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Panier;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import utils.MyDB;

/**
 *
 * @author bhk
 */
public class ServicePanier {

    Connection cnx;
    public ServicePanier() {
        cnx= MyDB.getInstance().getCnx();
    }
     
    public void addProductInCart(Panier p) throws SQLException{
        String req= "INSERT INTO `Panier` (`idproduit`, `qt`)"
                    + " VALUES ('"+p.getIdproduit()
                    +"', '"+p.getQt()+"')";
            Statement stm = cnx.createStatement();
            stm.executeUpdate(req);
    }
    
    public ArrayList<Panier> getAllProductsincart() throws SQLException{
        ArrayList<Panier> paniers = new ArrayList<>();
        String req= "SELECT * FROM `Panier`";
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        
        while (rst.next()){
            Panier p = new Panier();
            p.setId(rst.getInt(1));
            p.setIdproduit(rst.getInt("idproduit"));
            p.setQt(rst.getInt(3));
            paniers.add(p);
        }
        
        
        return paniers;
    }
    
    public void deleteProductInCart(int idproduct) throws SQLException{
        String req="delete from `Panier` where idproduit="+idproduct;
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
    public void deletePanier() throws SQLException{
        String req="delete from `Panier` ";
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
    
    public Panier GetProductInCart(int idproduct) throws SQLException{
        Panier p = new Panier();
        String req = "Select * from Panier where idproduit="+idproduct;
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        
        while (rst.next()){
            p.setId(rst.getInt(1));
            p.setIdproduit(rst.getInt("idproduit"));
            p.setQt(rst.getInt(3));
        }
        return p;
    }
    
    public boolean FindProductInCart(int idproduct) throws SQLException{
        boolean find=false;
        String req = "Select * from Panier where idproduit="+idproduct;
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
         while (rst.next()){
            find=true;
        }
        return find;
    }
    
    public void incrementQtInCart(int idproduct) throws SQLException{
        Panier p=this.GetProductInCart(idproduct);
        p.setQt(p.getQt()+1);
        this.deleteProductInCart(idproduct);
        this.addProductInCart(p);
    }
    public void decrementQtInCart(int idproduct) throws SQLException{
        Panier p=this.GetProductInCart(idproduct);
        p.setQt(p.getQt()-1);
        this.deleteProductInCart(idproduct);
        this.addProductInCart(p);
    }
    
}
