/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Commandeachat;
import entities.Commandevente;
import entities.Panier;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import utils.MyDB;

/**
 *
 * @author asus
 */
public class ServiceCommande {
    Connection cnx;
    public ServiceCommande() {
        cnx= MyDB.getInstance().getCnx();
    }
    public void addCommandeVente(Commandevente cv) throws SQLException{
        String req= "INSERT INTO `commandevente` (`idclient`, `idlivreur`,`total`,`date`,`etat`,`tempsvalidation`,`tempsrestant`,`rating`)"
                    + " VALUES ('"+cv.getIdclient()
                    +"', '"+cv.getIdlivreur()
                    +"', '"+cv.getTotal()
                    +"', '"+cv.getDate()
                    +"', '"+cv.getEtat()
                    +"', '"+cv.getTempsvalidation()
                    +"', '"+cv.getTempsrestant()
                    +"', '"+cv.getRating()+"')";
            Statement stm = cnx.createStatement();
            stm.executeUpdate(req);
    }
    public void addCommandeAchat(Commandeachat ca) throws SQLException{
        String req= "INSERT INTO `commandevente` (`idfournisseur`, `idresponsable`,`total`,`date`,`etat`)"
                    + " VALUES ('"+ca.getIdfournisseur()
                    +"', '"+ca.getIdresponsable()
                    +"', '"+ca.getTotal()
                    +"', '"+ca.getDate()
                    +"', '"+ca.getEtat()+"')";
            Statement stm = cnx.createStatement();
            stm.executeUpdate(req);
    }
    public void deleteCommandeVente(int idc) throws SQLException{
        String req="delete from `commandevente` where idproduit="+idc;
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
    public void deleteCommandeAchat(int idc) throws SQLException{
        String req="delete from `commandeachat` where idproduit="+idc;
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
    
    public ArrayList<Commandevente> getAllCommandeVente() throws SQLException{
        ArrayList<Commandevente> cvs = new ArrayList<>();
        String req= "SELECT * FROM `commandevente`";
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        
        while (rst.next()){
            Commandevente cv = new Commandevente();
            cv.setIdclient(rst.getInt("idclient"));
            cv.setIdlivreur(rst.getInt("idlivreur"));
            cv.setId(rst.getInt("id"));
            cv.setTotal(rst.getFloat("total"));
            cv.setDate(rst.getDate("date"));
            cv.setEtat(rst.getString("etat"));
            cv.setTempsvalidation(rst.getDate("tempsvalidation"));
            cv.setTempsrestant(rst.getString("tempsrestant"));
            cv.setRating(rst.getInt("rating"));
            cvs.add(cv);
        }
        return cvs;
    }
    public ArrayList<Commandeachat> getAllCommandeAchat() throws SQLException{
        ArrayList<Commandeachat> cas = new ArrayList<>();
        String req= "SELECT * FROM `commandeachat`";
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        
        while (rst.next()){
            Commandeachat ca = new Commandeachat();
            ca.setIdfournisseur(rst.getInt("idclient"));
            ca.setIdresponsable(rst.getInt("idlivreur"));
            ca.setId(rst.getInt("id"));
            ca.setTotal(rst.getFloat("total"));
            ca.setDate(rst.getDate("date"));
            ca.setEtat(rst.getString("etat"));
            cas.add(ca);
        }
        return cas;
    }
    public Commandevente getCommandeVenteById(int idc) throws SQLException{
        String req= "SELECT * FROM `commandevente` where id="+idc;
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        Commandevente cv = new Commandevente();
        while (rst.next()){
              cv.setIdclient(rst.getInt("idclient"));
              cv.setIdlivreur(rst.getInt("idlivreur"));
              cv.setId(rst.getInt("id"));
              cv.setTotal(rst.getFloat("total"));
              cv.setDate(rst.getDate("date"));
              cv.setEtat(rst.getString("etat"));
              cv.setTempsvalidation(rst.getDate("tempsvalidation"));
              cv.setTempsrestant(rst.getString("tempsrestant"));
              cv.setRating(rst.getInt("rating"));
          }
        return cv;
    }
    public Commandeachat getCommandeAchatById(int idc) throws SQLException{
        String req= "SELECT * FROM `commandeachat` where id="+idc;
        Statement stm = cnx.createStatement();
        ResultSet rst= stm.executeQuery(req);
        Commandeachat ca = new Commandeachat();
        while (rst.next()){
            ca.setIdfournisseur(rst.getInt("idclient"));
            ca.setIdresponsable(rst.getInt("idlivreur"));
            ca.setId(rst.getInt("id"));
            ca.setTotal(rst.getFloat("total"));
            ca.setDate(rst.getDate("date"));
            ca.setEtat(rst.getString("etat"));
          }
        return ca;
    }
}
