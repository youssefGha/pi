/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Commandeachat;
import entities.Commandevente;
import entities.Panier;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import utils.MyDB;

/**
 *
 * @author asus
 */
public class ServiceLigne {
    Connection cnx;
    ServicePanier sp = new ServicePanier();
    public ServiceLigne() {
        cnx= MyDB.getInstance().getCnx();
    }
    public void addLigneVente(Commandevente cv) throws SQLException{
        ArrayList<Panier> paniers = new ArrayList<>();
        paniers=sp.getAllProductsincart();
        for (Panier p : paniers){
            String req= "INSERT INTO `ligneventes` (`idproduit`, `idcommande`,`pu`,`qt`)"
                    + " VALUES ('"+p.getIdproduit()
                    +"', '"+cv.getId()
                    +"', '"+"1"//temporaire jusqu'a ce que le crud du produit soit prêt
                    +"', '"+p.getQt()+"')";
            Statement stm = cnx.createStatement();
            stm.executeUpdate(req);
        }
        sp.deletePanier();
    }
    public void addLigneAchat(Commandeachat ca) throws SQLException{
        ArrayList<Panier> paniers = new ArrayList<>();
        paniers=sp.getAllProductsincart();
        for (Panier p : paniers){
            String req= "INSERT INTO `ligneventes` (`idproduit`, `idcommande`,`pu`,`qt`)"
                    + " VALUES ('"+p.getIdproduit()
                    +"', '"+ca.getId()
                    +"', '"+"1"//temporaire jusqu'a ce que le crud du produit soit prêt
                    +"', '"+p.getQt()+"')";
            Statement stm = cnx.createStatement();
            stm.executeUpdate(req);
        }
        sp.deletePanier();
    }
    public void deleteLigneVente(int idcommande) throws SQLException{
        String req="delete from `ligneventes` where idcommande="+idcommande;
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
    public void deleteLigneAchat(int idcommande) throws SQLException{
        String req="delete from `ligneachats` where idcommande="+idcommande;
        Statement stm = cnx.createStatement();
        stm.executeUpdate(req);
    }
}
