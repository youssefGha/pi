/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author asus
 */
public class Produit {
    
    private int idcategorie;
    private int id;
    private String lib;
    private float prixvente;
    private float prixachat;
    private String disponibilite;
    private String description;
    private String image;

    public int getIdcategorie() {
        return idcategorie;
    }

    public void setIdcategorie(int idcategorie) {
        this.idcategorie = idcategorie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLib() {
        return lib;
    }

    public void setLib(String lib) {
        this.lib = lib;
    }

    public float getPrixvente() {
        return prixvente;
    }

    public void setPrixvente(float prixvente) {
        this.prixvente = prixvente;
    }

    public float getPrixachat() {
        return prixachat;
    }

    public void setPrixachat(float prixachat) {
        this.prixachat = prixachat;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    
}
