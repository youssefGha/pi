/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author asus
 */
public class Commandevente {
    
    private int id;
    private int idclient;
    private int idlivreur;
    private float total;
    private Date date;
    private String etat;
    private Date tempsvalidation;
    private String tempsrestant;
    private int rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public int getIdclient() {
        return idclient;
    }

    public void setIdclient(int idclient) {
        this.idclient = idclient;
    }

    public int getIdlivreur() {
        return idlivreur;
    }

    public void setIdlivreur(int idlivreur) {
        this.idlivreur = idlivreur;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Date getTempsvalidation() {
        return tempsvalidation;
    }

    public void setTempsvalidation(Date tempsvalidation) {
        this.tempsvalidation = tempsvalidation;
    }

    public String getTempsrestant() {
        return tempsrestant;
    }

    public void setTempsrestant(String tempsrestant) {
        this.tempsrestant = tempsrestant;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
    
    
}
